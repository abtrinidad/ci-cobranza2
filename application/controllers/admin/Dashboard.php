<?php

class Dashboard extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['subview'] = 'admin/index';
        $this->load->view('_main_layout',$data);
    }
}